These notebooks in Python 3.6 illustrate the framework described in the following paper:
"MOU-EC: model-based whole-brain effective connectivity to extract biomarkers for brain dynamics from fMRI data and study distributed cognition"
Matthieu Gilson, Gorka Zamora-Lopez, Vicente Pallares, Mohit H Adhikari, Mario Senden, Adria Tauste Campo, Dante Mantini, Maurizio Corbetta, Gustavo Deco, Andrea Insabato
bioRxiv, http://doi.org/10.1101/531830 

## Whole-brain linear effective connectivity (WBLEC) estimation:
The notebook MOUEC_estimation.ipynb calculates the spatiotemporal functional connectivity for each session (or run) and subject from the BOLD time series. Then, it calls the model optimization (function in MOUmodel.py) and stores the model estimates (effective connectivity matrix embedded in the Jacobian J and input variances Sigma) in an array. The data are:
- BOLD time series in ts_emp.npy
- structural connectivity in SC_anat.npy
- ROI labels in ROI_labels.npy

## Classification:
The notebook Classification.ipynb compares the performances of two classifiers (multinomial linear regressor and 1-nearest-neighbor) in identifying subjects from EC taken as a biomarker.

## Dynamic Communicability:
The notebook Dynamic_Communicability.ipynb calculates network measures based on the estimated Jacobian matrices (for each fMRI session) to interpret the BOLD signals in a dynamic fashion. For example, it describes how the ROIs listen and broadcast information (quantified via the BOLD activity) in the brain network.

## Further references:

Data are from: Gilson M, Deco G, Friston K, Hagmann P, Mantini D, Betti V, Romani GL, Corbetta M. Effective connectivity inferred from fMRI transition dynamics during movie viewing points to a balanced reconfiguration of cortical interactions. Neuroimage 2017; doi.org/10.1016/j.neuroimage.2017.09.061

The model optimization is described in: Gilson M, Moreno-Bote R, Ponce-Alvarez A, Ritter P, Deco G. Estimation of Directed Effective Connectivity from fMRI Functional Connectivity Hints at Asymmetries of Cortical Connectome. PLoS Comput Biol 2016, 12: e1004762; dx.doi.org/10.1371/journal.pcbi.1004762

The classification procedure is described in: Pallares V, Insabato A, Sanjuan A, Kuehn S, Mantini D, Deco G, Gilson M. Subject- and behavior-specific signatures extracted from fMRI data using whole-brain effective connectivity. Biorxiv, doi.org/10.1101/201624
The classification script uses the scikit.learn library (http://scikit-learn.org)

The application of dynamic communicability to fMRI is detailed in: Gilson M, Kouvaris NE, Deco G, Mangin J-F, Poupon C, Lefranc S, Rivière D, Zamora-López G. Network analysis of whole-brain fMRI dynamics: A new framework based on dynamic communicability. Biorxiv; http:doi.org/10.1101/421883